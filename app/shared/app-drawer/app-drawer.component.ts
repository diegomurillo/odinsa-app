import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { RadSideDrawerComponent } from 'nativescript-telerik-ui/sidedrawer/angular';
import { RadSideDrawer } from 'nativescript-telerik-ui/sidedrawer';

@Component({
  moduleId: module.id,
  selector: 'AppDrawer',
  templateUrl: './app-drawer.component.html',
  styleUrls: ['./app-drawer.component.css']
})
export class AppDrawerComponent implements OnInit, AfterViewInit {
  @ViewChild(RadSideDrawerComponent) public drawerComponent: RadSideDrawerComponent;
  private drawer: RadSideDrawer;

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.drawer = this.drawerComponent.sideDrawer;
  }

  public openDrawer() {
    this.drawer.showDrawer();
  }

}