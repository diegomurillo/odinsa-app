import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppDrawerComponent } from './app-drawer/app-drawer.component';
import { NativeScriptUISideDrawerModule } from "nativescript-telerik-ui/sidedrawer/angular";
import { UserModule } from '../user/user.module';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { APP_DI_CONFIG, APP_CONFIG } from './app-config.constants';

@NgModule({
  imports: [
    CommonModule,
    NativeScriptRouterModule,
    NativeScriptUISideDrawerModule
  ],
  exports: [
    AppDrawerComponent
  ],
  declarations: [AppDrawerComponent],
  providers: [
    {
      provide: APP_CONFIG,
      useValue: APP_DI_CONFIG
    }
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class SharedModule { }