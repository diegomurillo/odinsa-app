import { AppLocalStorage } from '../utils/local-storage.interface';
import 'reflect-metadata';
require('nativescript-localstorage');

const PROPERTIES_TO_SAVE_TO_LOCALSTORAGE: string = "PROPERTIES_TO_SAVE_TO_LOCALSTORAGE"

export function LocalStorageVO() {
    return function <T extends { new (...args: any[]): {} }>(constructor: T) {
        return class extends constructor {
            saveToLocalStorage() {
                let properties = Reflect.getMetadata(PROPERTIES_TO_SAVE_TO_LOCALSTORAGE, constructor.prototype) as Array<string>
                let data = {}
                properties.forEach((value) => {
                    data[value] = this[value]
                })
                localStorage.setItem(constructor.name, JSON.stringify(data))
            }

            loadFromLocalStorage() {
                let data = <T>JSON.parse(localStorage.getItem(constructor.name))
                console.dir(data)
                Object.assign(this, data)
            }

            removeFromLocalStorage(){
                localStorage.removeItem(constructor.name)
            }

            clearLocalStorage() {
                localStorage.clear()
            }
        }
    }
}

export function LocalStorageField() {
    return function (target: any, propertyKey: string) {
        console.log(propertyKey)
        if (!Reflect.hasMetadata(PROPERTIES_TO_SAVE_TO_LOCALSTORAGE, target)) {
            Reflect.defineMetadata(PROPERTIES_TO_SAVE_TO_LOCALSTORAGE, [propertyKey], target)
        } else {
            let properties = Reflect.getMetadata(PROPERTIES_TO_SAVE_TO_LOCALSTORAGE, target) as Array<string>
            properties.push(propertyKey)
            Reflect.defineMetadata(PROPERTIES_TO_SAVE_TO_LOCALSTORAGE, properties, target)
        }
    }
}