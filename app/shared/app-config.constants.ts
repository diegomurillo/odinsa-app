import { InjectionToken } from "@angular/core";
import { AppConfig } from "./app-config.interface";

export const APP_DI_CONFIG: AppConfig = {

  API_URL: '' // TODO: define API URL for RELEASE

};

export let APP_CONFIG = new InjectionToken< AppConfig >( 'app.config' );