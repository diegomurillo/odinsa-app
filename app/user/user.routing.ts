import { Routes, Router } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { NativeScriptRouterModule } from 'nativescript-angular/router';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'profile', component: ProfileComponent}
];

export const UserRoutes = NativeScriptRouterModule.forChild(routes);
