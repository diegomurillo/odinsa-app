import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile/profile.component';
import { HomeComponent } from './home/home.component';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { UserRoutes } from './user.routing';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    NativeScriptRouterModule,
    UserRoutes,
    SharedModule
  ],
  exports: [
    NativeScriptRouterModule
  ],
  declarations: [
    ProfileComponent,
    HomeComponent
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class UserModule { }