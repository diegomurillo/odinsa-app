import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { Page } from 'tns-core-modules/ui/page';
import { Color } from 'color'
import { AppConfig } from '../../shared/app-config.interface';
import { APP_CONFIG } from '../../shared/app-config.constants';

@Component({
  moduleId: module.id,
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  constructor( /*@Inject(APP_CONFIG) private config: AppConfig,*/ private router: Router, private page: Page) { }

  ngOnInit() {
    this.setupPage()
  }

  private setupPage() {
    this.page.actionBarHidden = true;
  }

  public doSignIn() {

  }

}