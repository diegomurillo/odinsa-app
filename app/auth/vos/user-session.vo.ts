
import { AppLocalStorage } from '../../shared/utils/local-storage.interface';
import { LocalStorageVO, LocalStorageField } from '../../shared/decorators/local-storage.decorators';
import { Injectable } from '@angular/core';

@Injectable()
@LocalStorageVO()
export class UserSession extends AppLocalStorage{
    email:string = null
    password:string = null
    @LocalStorageField()
    token:string = null

    constructor(){
        super()
        this.loadFromLocalStorage()
    }
}