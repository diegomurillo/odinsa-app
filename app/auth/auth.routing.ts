import { Routes } from '@angular/router';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { NativeScriptRouterModule } from 'nativescript-angular/router'

const routes: Routes = [
  { path: 'login', component: SignInComponent },
  { path: 'register', component: SignUpComponent },
];

export const AuthRoutes = NativeScriptRouterModule.forChild(routes);
