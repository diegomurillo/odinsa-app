import { Injectable } from '@angular/core';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from 'rxjs/Rx';
import { UserSession } from './vos/user-session.vo';


@Injectable()
export class AuthService {

    constructor(private http: Http, private userSession: UserSession) { }

    public makeSignIn(username: string, password: string): Observable<Response> {
        return this.http.post('', {}, {})
    }

    public isUserSignedIn(): boolean {
        return this.userSession.token !== null
    }

}