import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { AuthRoutes } from './auth.routing';
import { NativeScriptRouterModule } from 'nativescript-angular/router'
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { UserSession } from './vos/user-session.vo';

@NgModule({
  imports: [
    CommonModule,
    AuthRoutes,
    AuthRoutes,
    NativeScriptRouterModule,
    NativeScriptHttpModule
  ],
  exports: [
    NativeScriptRouterModule
  ],
  declarations: [
    SignInComponent,
    SignUpComponent
  ],
  providers: [
    UserSession
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class AuthModule { }