import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from 'nativescript-angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
];

export const AppRoutes = NativeScriptRouterModule.forRoot(routes);
